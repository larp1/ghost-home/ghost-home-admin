import { Role } from '@entities';
import { RolesFeatures } from '@features';
import { FC } from 'react';
import styled from 'styled-components';

export const RolesPage: FC = () => {
  return (
    <div>
      <AddRole />
      <Role.TableRoles actions={[RolesFeatures.UpdateRole.Button, RolesFeatures.RemoveRole.Button]} />
    </div>
  );
};

const AddRole = styled(RolesFeatures.AddRole.UI)`
  margin-bottom: 30px;
`;
