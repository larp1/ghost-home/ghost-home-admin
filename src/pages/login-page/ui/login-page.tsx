import { api } from '@shared/api';
import { AppRoutes } from '@shared/routes';
import { Button } from 'antd';
import { FC } from 'react';
import { useNavigate } from 'react-router-dom';
import styled from 'styled-components';

// TODO: remove this page
export const LoginPage: FC = () => {
  const navigate = useNavigate();

  const postRole = async () => {
    try {
      const result = await api.RolesApi.createRoleModerationRolePost({ name: 'TEST' });
      console.log('result', result);
    } catch (e) {
      console.log('e', e);
    }
  };

  const getRoles = async () => {
    try {
      const result = await api.RolesApi.getRolesModerationRolesGet();
      console.log('result', result);
    } catch (e) {
      console.log('e', e);
    }
  };

  return (
    <Root>
      <>
        Login Page: {test}
        <p>
          <Button onClick={() => navigate(AppRoutes.RolesPage, { replace: true })}>LOG IN</Button>
          <Button onClick={postRole}>Create</Button>
          <Button onClick={getRoles}>Get</Button>
        </p>
      </>
    </Root>
  );
};

const Root = styled.div``;
