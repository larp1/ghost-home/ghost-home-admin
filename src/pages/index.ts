import * as CluesPage from './clues-page';
import * as LoginPage from './login-page';
import * as RolesPage from './roles-page';
import * as UsersPage from './users-page';

export { RolesPage, LoginPage, UsersPage, CluesPage };
