import { User } from '@entities';
import { UsersFeatures } from '@features';
import React, { FC } from 'react';
import styled from 'styled-components';

export const UsersPage: FC = () => {
  return (
    <div>
      <TopRow>
        <UsersFeatures.UpdateList.Button />
        <UsersFeatures.CreateUser.Button />
      </TopRow>
      <User.TableUsers
        actions={[
          UsersFeatures.UpdateUser.Button,
          UsersFeatures.RemoveUser.Button,
          UsersFeatures.GoGamerProfile.Button,
          UsersFeatures.QRCode.QRButton,
        ]}
      />
    </div>
  );
};

const TopRow = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 20px;
`;
