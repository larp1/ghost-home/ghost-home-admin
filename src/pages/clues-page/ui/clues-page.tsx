import { Clue } from '@entities';
import { CluesFeatures } from '@features';
import React, { FC } from 'react';
import styled from 'styled-components';

export const CluesPage: FC = () => {
  return (
    <div>
      <TopRow>
        <CluesFeatures.UpdateList.Button />
        <CluesFeatures.CreateClue.Button />
      </TopRow>
      <Clue.TableClues
        actions={[
          CluesFeatures.UpdateClue.Button,
          CluesFeatures.RemoveClue.Button,
          CluesFeatures.Links.ManageLinks,
          CluesFeatures.QRCode.QRButton,
        ]}
      />
    </div>
  );
};

const TopRow = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 20px;
`;
