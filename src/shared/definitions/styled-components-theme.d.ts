import {} from 'styled-components';

import { ThemeType } from '@shared/themes';

declare module 'styled-components' {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  export interface DefaultTheme extends ThemeType {}
}

declare module '*.json' {
  const value: any; // eslint-disable-line @typescript-eslint/no-explicit-any
  export default value;
}
