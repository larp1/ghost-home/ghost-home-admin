import { API_BASE_URL } from '@shared/constants';

import { ItemsApi, LinksApi, MadnessApi, RolesApi, UsersApi } from './__generated__';
import { httpClient } from './http-client';

export const api = {
  CluesApi: new ItemsApi(undefined, API_BASE_URL, httpClient),
  LinksApi: new LinksApi(undefined, API_BASE_URL, httpClient),
  RolesApi: new RolesApi(undefined, API_BASE_URL, httpClient),
  UsersApi: new UsersApi(undefined, API_BASE_URL, httpClient),
  MadnessApi: new MadnessApi(undefined, API_BASE_URL, httpClient),
};
