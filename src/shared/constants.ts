export enum EnvironmentType {
  development = 'development',
  production = 'production',
}

export const ENVIRONMENT = process.env.REACT_APP_ENVIRONMENT;
export const API_BASE_URL = process.env.REACT_APP_API_BASE_URL;

export enum APP_LANGUAGES {
  RU = 'ru',
}
export const DEFAULT_LANGUAGE = APP_LANGUAGES.RU;
