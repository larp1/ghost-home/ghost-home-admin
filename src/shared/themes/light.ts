export const lightTheme = {
  name: 'dark',
  colors: {
    white: '#FFFFFF',
    grey: '#696B72',
    black: '#000000',
  },
  typography: {
    title57: {
      fontSize: '57px',
      fontWeight: 700,
      lineHeight: '69px',
    },
  },
};
