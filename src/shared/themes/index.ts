import { lightTheme } from './light';

const defaultTheme = { ...lightTheme, name: 'default' };

export type ThemesNames = 'default' | 'light';
export type ThemeType = typeof defaultTheme;

const themesMap: Record<ThemesNames, ThemeType> = {
  default: defaultTheme,
  light: lightTheme,
};

export { lightTheme, defaultTheme, themesMap };
export default defaultTheme;
