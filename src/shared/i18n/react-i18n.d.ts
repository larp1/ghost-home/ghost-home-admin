import 'react-i18next';

import { ru } from './lang/ru.json';

declare module 'react-i18next' {
  interface CustomTypeOptions {
    defaultNS: 'ru';

    resources: {
      ru: typeof ru.translation;
    };
  }
}
