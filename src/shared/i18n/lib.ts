import { APP_LANGUAGES, DEFAULT_LANGUAGE } from '@shared/constants';
import i18n from 'i18next';
import { Normalize, initReactI18next } from 'react-i18next';

import ruJson from './lang/ru.json';

export const resources = {
  ru: ruJson.ru,
};

i18n.use(initReactI18next).init({
  compatibilityJSON: 'v3',
  lng: DEFAULT_LANGUAGE,
  resources,
  missingInterpolationHandler: function () {},
  interpolation: {
    escapeValue: false,
  },
});

export const changeLanguage = (lang: APP_LANGUAGES) => {
  i18n.changeLanguage(lang);
};

export default i18n;

const translation = ruJson.ru.translation;
export type TranslationObject = typeof translation;
export type KeysTranslation = Normalize<typeof translation>;
