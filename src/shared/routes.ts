export enum AppRoutes {
  BaseUrl = '/',
  LoginPage = '/login',
  RolesPage = '/roles',
  UsersPage = '/users',
  CluesPage = '/clues',
}
