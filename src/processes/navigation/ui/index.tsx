import { CluesPage, RolesPage, UsersPage } from '@pages';
import { AppRoutes } from '@shared/routes';
import { DefaultLayout } from '@widgets';
import { FC } from 'react';
import { MemoryRouter, Route, Routes } from 'react-router-dom';

export const Navigator: FC = () => {
  return (
    <MemoryRouter initialIndex={0} initialEntries={[AppRoutes.RolesPage]}>
      <Routes>
        {/* <Route path={AppRoutes.LoginPage} element={<LoginPage.PageUI />} /> */}
        <Route path={AppRoutes.BaseUrl} element={<DefaultLayout.UI />}>
          <Route path={AppRoutes.RolesPage} element={<RolesPage.PageUI />} />
          <Route path={AppRoutes.UsersPage} element={<UsersPage.PageUI />} />
          <Route path={AppRoutes.CluesPage} element={<CluesPage.PageUI />} />
        </Route>
      </Routes>
    </MemoryRouter>
  );
};
