import * as slice from './slice';
import { useTableRoles } from './use-table-roles';

export { slice, useTableRoles };
