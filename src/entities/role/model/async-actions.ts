import { createAsyncThunk } from '@reduxjs/toolkit';
import { RoleFullInfo, api } from '@shared/api';

export const getRoles = createAsyncThunk('roles/get-roles', async () => {
  try {
    const { data } = await api.RolesApi.getRolesModerationRolesGet();
    return data;
  } catch (e) {
    // TODO: check handle error
    if (typeof e === 'string') {
      throw new Error(e);
    }
    throw new Error('Something went wrong');
  }
});

export const getRoleById = createAsyncThunk('roles/get-role-by-id', async (roleId: number) => {
  try {
    const { data } = await api.RolesApi.getRoleModerationRoleRoleIdGet(roleId);
    return data;
  } catch (e) {
    // TODO: check handle error
    if (typeof e === 'string') {
      throw new Error(e);
    }
    throw new Error('Something went wrong');
  }
});

export const updateRole = createAsyncThunk('roles/update-role', async (role: RoleFullInfo) => {
  try {
    const { data } = await api.RolesApi.updateRoleModerationRoleRoleIdPut(role.id, {
      name: role.name,
      description: role.description || '',
    });
    return data;
  } catch (e) {
    // TODO: check handle error
    if (typeof e === 'string') {
      throw new Error(e);
    }
    throw new Error('Something went wrong');
  }
});

export const removeRole = createAsyncThunk('roles/remove-role', async (id: number) => {
  try {
    await api.RolesApi.deleteRoleModerationRoleRoleIdDelete(id);
    return id;
  } catch (e) {
    // TODO: check handle error
    if (typeof e === 'string') {
      throw new Error(e);
    }
    throw new Error('Something went wrong');
  }
});
