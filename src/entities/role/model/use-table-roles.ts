import { useAppDispatch } from '@app';
import { unwrapResult } from '@reduxjs/toolkit';
import { useCallback, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import { DataType } from '../types';
import { actions, selectors } from './slice';

export const useTableRoles = () => {
  const dispatch = useAppDispatch();
  const roles = useSelector(selectors.selectRoles);
  const [loading, setLoading] = useState(false);
  const [loadingById, setLoadingById] = useState<number | undefined>();

  useEffect(() => {
    const getRoles = async () => {
      try {
        setLoading(true);
        const actionResult = await dispatch(actions.getRoles());
        unwrapResult(actionResult);
      } catch (e) {
        // TODO: remove console add toastr
        console.warn('error from get roles', e);
      } finally {
        setLoading(false);
      }
    };

    getRoles();
  }, [dispatch]);

  const getRoleById = useCallback(
    async (expanded: boolean, record: DataType) => {
      if (expanded) {
        try {
          setLoadingById(record.id);
          const actionResult = await dispatch(actions.getRoleById(record.id));
          unwrapResult(actionResult);
        } catch (e) {
          // TODO: remove console add toastr
          console.warn('error from get one roles', e);
        } finally {
          setLoadingById(undefined);
        }
      }
    },
    [dispatch],
  );

  return { data: { roles, loading }, oneRole: { getRoleById, loadingById } };
};
