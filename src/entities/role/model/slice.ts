import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { RoleFullInfo, RoleListItem } from '@shared/api';

import { getRoleById, getRoles, removeRole, updateRole } from './async-actions';

type RolesState = {
  items: RoleFullInfo[];
};

export type RolesInStore = {
  roles: RolesState;
};

const initialState: RolesState = {
  items: [],
};

const rolesSlice = createSlice({
  name: 'roles',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getRoles.fulfilled.type, (state, { payload }: PayloadAction<RoleListItem[]>) => {
      state.items = payload.reverse();
    });
    builder.addCase(getRoleById.fulfilled.type, (state, { payload }: PayloadAction<RoleFullInfo>) => {
      const role = state.items.find((r) => r.id === payload.id);
      if (role) {
        role.name = payload.name;
        role.description = payload.description;
      }
    });
    builder.addCase(updateRole.fulfilled.type, (state, { payload }: PayloadAction<RoleFullInfo>) => {
      const role = state.items.find((r) => r.id === payload.id);
      if (role) {
        role.name = payload.name;
        role.description = payload.description;
      }
    });
    builder.addCase(removeRole.fulfilled.type, (state, { payload }: PayloadAction<number>) => {
      state.items = state.items.filter((item) => item.id !== payload);
    });
  },
});

export const selectors = {
  selectRoles: (state: RolesInStore) => state.roles.items,
  selectRolesById: (id: number) => (state: RolesInStore) => state.roles.items.find((i) => i.id === id),
  selectRolesMap: (state: RolesInStore) =>
    state.roles.items.reduce((acc, role) => {
      return { ...acc, [role.name]: role.id };
    }, {}) as Record<string, number>,
};
export const actions = { ...rolesSlice.actions, getRoles, getRoleById, updateRole, removeRole };
export const { reducer } = rolesSlice;
