export type DataType = {
  id: number;
  key: React.Key;
  name: string;
  description?: string;
};
