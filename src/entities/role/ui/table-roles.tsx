import { Space, Table } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { ExpandableConfig } from 'antd/es/table/interface';
import { FC, useCallback, useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import { useTableRoles } from '../model';
import { DataType } from '../types';
import { ActionRow } from './components';

const SCROLL_TABLE = { y: 'calc(100vh - 180px)' };

type TableRolesProps = {
  actions: FC<{ id: number }>[];
};

export const TableRoles: FC<TableRolesProps> = ({ actions }) => {
  const { t } = useTranslation();
  const { data, oneRole } = useTableRoles();

  const columns: ColumnsType<DataType> = useMemo(
    () => [
      {
        title: t('entities.role.table.name'),
        dataIndex: 'name',
        key: 'name',
        defaultSortOrder: 'descend',
        sorter: (a, b) => (a.name > b.name ? 1 : -1),
      },
      {
        title: t('entities.role.table.actions'),
        dataIndex: '',
        key: 'action',
        render: (_, record) => (
          <Space size="small">{<ActionRow id={record.id} components={actions} />}</Space>
        ),
        width: '112px',
      },
    ],
    [t, actions],
  );

  const tableData: DataType[] = useMemo(() => {
    return data.roles.map((role) => ({
      key: role.id,
      name: role.name,
      description: role.description,
      id: role.id,
    }));
  }, [data.roles]);

  const renderDescriptionRow = useCallback((record: DataType) => {
    return <p>{record.description}</p>;
  }, []);

  const expandable: ExpandableConfig<DataType> = useMemo(
    () => ({
      expandedRowRender: renderDescriptionRow,
      onExpand: oneRole.getRoleById,
    }),
    [oneRole.getRoleById, renderDescriptionRow],
  );

  return (
    <Table
      columns={columns}
      dataSource={tableData}
      loading={data.loading}
      pagination={false}
      expandable={expandable}
      scroll={SCROLL_TABLE}
    />
  );
};
