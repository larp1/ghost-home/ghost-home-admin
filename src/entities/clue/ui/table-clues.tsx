import { Role } from '@entities';
import { Space, Table } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { FC, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { useTableClues } from '../model';
import { DataType } from '../types';
import { ActionRow } from './components';

const SCROLL_TABLE = { y: 'calc(100vh - 180px)' };

type TableUsersProps = {
  actions: FC<{ id: number }>[];
};

export const TableClues: FC<TableUsersProps> = ({ actions }) => {
  const { t } = useTranslation();
  const { clues, loading } = useTableClues();
  const roles = useSelector(Role.slice.selectors.selectRoles);

  const columns: ColumnsType<DataType> = useMemo(
    () => [
      {
        title: t('entities.clue.table.title'),
        dataIndex: 'title',
        key: 'title',
        sorter: (a, b) => (a.title > b.title ? 1 : -1),
      },
      {
        title: t('entities.clue.table.math'),
        dataIndex: 'math',
        key: 'math',
      },
      {
        title: t('entities.clue.table.role'),
        dataIndex: 'role',
        key: 'role',
        render: (_, record) => <Space>{roles.find((role) => role.id === record.role)?.name}</Space>,
        defaultSortOrder: 'ascend',
        sorter: (a, b) => a.role - b.role,
      },
      {
        title: t('entities.clue.table.madness'),
        dataIndex: 'madness',
        key: 'madness',
      },
      {
        title: t('entities.clue.table.actions'),
        dataIndex: '',
        key: 'action',
        render: (_, record) => (
          <Space size="small">{<ActionRow id={record.id} components={actions} />}</Space>
        ),
        width: '200px',
      },
    ],
    [t, actions, roles],
  );

  const tableData: DataType[] = useMemo(() => {
    return clues
      .map((clue) => ({
        key: clue.id,
        title: clue.title,
        text: clue.text,
        role: clue.role,
        math: clue.math,
        madness: clue.madnes,
        id: clue.id,
      }))
      .reverse();
  }, [clues]);

  return (
    <Table
      columns={columns}
      dataSource={tableData}
      loading={loading}
      pagination={false}
      scroll={SCROLL_TABLE}
    />
  );
};
