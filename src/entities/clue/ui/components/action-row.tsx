import React, { FC, useMemo } from 'react';
import styled from 'styled-components';

export type ActionRowProps = {
  id: number;
  components: FC<{ id: number }>[];
};

const ActionRow: FC<ActionRowProps> = ({ id, components }) => {
  return (
    <Root>
      {useMemo(() => components.map((Item, index) => <Item key={index} id={id} />), [id, components])}
    </Root>
  );
};

export default ActionRow;

const Root = styled.div`
  > * {
    &:not(:last-child) {
      margin-right: 10px;
    }
  }
`;
