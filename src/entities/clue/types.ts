export type DataType = {
  id: number;
  key: React.Key;
  title: string;
  math?: string;
  madnes?: number;
  role: number;
  text: string;
};
