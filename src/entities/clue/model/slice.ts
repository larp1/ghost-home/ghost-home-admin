import { createSlice } from '@reduxjs/toolkit';
import { FullItem } from '@shared/api';

import {
  createClue,
  createLink,
  deleteClue,
  deleteLink,
  getAllClues,
  getClueById,
  updateClue,
  updateLink,
} from './async-actions';

type ClueState = {
  items: FullItem[];
};

export type CluesInStore = {
  clues: ClueState;
};

const initialState: ClueState = {
  items: [],
};

const cluesSlice = createSlice({
  name: 'clues',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    // clues
    builder.addCase(getAllClues.fulfilled, (state, { payload }) => {
      state.items = payload;
    });
    builder.addCase(getClueById.fulfilled, (state, { payload }) => {
      const clueIndex = state.items.findIndex((i) => i.id === payload.id);
      if (clueIndex !== -1) {
        state.items[clueIndex] = payload;
      }
    });
    builder.addCase(createClue.fulfilled, (state, { payload }) => {
      state.items.push(payload);
    });
    builder.addCase(updateClue.fulfilled, (state, { payload }) => {
      const clueIndex = state.items.findIndex((i) => i.id === payload.id);
      if (clueIndex !== -1) {
        state.items[clueIndex] = payload;
      }
    });
    builder.addCase(deleteClue.fulfilled, (state, { payload }) => {
      state.items = state.items.filter((item) => item.id !== payload);
    });
  },
});

export const selectors = {
  selectClues: (state: CluesInStore) => state.clues.items,
  selectClueById: (id: number) => (state: CluesInStore) => state.clues.items.find((i) => i.id === id),
  selectCluesMap: (state: CluesInStore) =>
    state.clues.items.reduce((acc, clue) => {
      return { ...acc, [clue.id]: clue.title };
    }, {}) as Record<number, string>,
};
export const actions = {
  ...cluesSlice.actions,
  createClue,
  createLink,
  deleteClue,
  deleteLink,
  getAllClues,
  getClueById,
  updateClue,
  updateLink,
};
export const { reducer } = cluesSlice;
