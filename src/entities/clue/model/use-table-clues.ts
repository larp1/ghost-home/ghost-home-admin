import { useAppDispatch } from '@app';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import { actions, selectors } from './slice';

export const useTableClues = () => {
  const dispatch = useAppDispatch();
  const clues = useSelector(selectors.selectClues);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const getClues = async () => {
      try {
        setLoading(true);
        await dispatch(actions.getAllClues()).unwrap();
      } catch (err) {
        // TODO: remove console add toastr
        console.warn('error from get users', err);
      } finally {
        setLoading(false);
      }
    };

    getClues();
  }, [dispatch]);

  return { clues, loading };
};
