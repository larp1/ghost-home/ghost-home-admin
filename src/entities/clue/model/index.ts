import * as slice from './slice';
import { useTableClues } from './use-table-clues';

export { slice, useTableClues };
