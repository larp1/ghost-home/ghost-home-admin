import { createAsyncThunk } from '@reduxjs/toolkit';
import { ItemLinkAdd, ItemLinkBase, ItemUpdate, ItemWithLink, api } from '@shared/api';

// clues
export const getAllClues = createAsyncThunk('clues/get-all-clues', async () => {
  try {
    const { data } = await api.CluesApi.getItemsModerationItemsGet();
    return data;
  } catch (e) {
    // TODO: check handle error
    if (typeof e === 'string') {
      throw new Error(e);
    }
    throw new Error('Something went wrong');
  }
});

export const getClueById = createAsyncThunk('clues/get-clue-by-id', async (id: number) => {
  try {
    const { data } = await api.CluesApi.getItemModerationItemItemIdGet(id);
    return data;
  } catch (e) {
    // TODO: check handle error
    if (typeof e === 'string') {
      throw new Error(e);
    }
    throw new Error('Something went wrong');
  }
});

export const createClue = createAsyncThunk(
  'clues/create-clue',
  async ({ role, text, title, links, madnes, math }: ItemWithLink) => {
    try {
      const { data } = await api.CluesApi.createItemModerationItemPost({
        role,
        text,
        title,
        links,
        madnes,
        math,
      });
      return data;
    } catch (e) {
      // TODO: check handle error
      if (typeof e === 'string') {
        throw new Error(e);
      }
      throw e;
    }
  },
);

export const updateClue = createAsyncThunk(
  'clues/update-clue',
  async ({ id, itemUpdate }: { id: number; itemUpdate: ItemUpdate }) => {
    try {
      const { data } = await api.CluesApi.updateItemModerationItemsItemIdPut(id, itemUpdate);
      return data;
    } catch (e) {
      // TODO: check handle error
      if (typeof e === 'string') {
        throw new Error(e);
      }
      throw new Error('Something went wrong');
    }
  },
);

export const deleteClue = createAsyncThunk('clues/delete-clue', async (id: number) => {
  try {
    await api.CluesApi.deleteItemModerationItemItemIdDelete(id);
    return id;
  } catch (e) {
    // TODO: check handle error
    if (typeof e === 'string') {
      throw new Error(e);
    }
    throw new Error('Something went wrong');
  }
});

// links
export const createLink = createAsyncThunk(
  'clues/create-link',
  async (itemLinkAdd: ItemLinkAdd, thunkApi) => {
    try {
      const { data } = await api.LinksApi.addLinkModerationLinkPost(itemLinkAdd);
      thunkApi.dispatch(getClueById(itemLinkAdd.owner_id));
      return data;
    } catch (e) {
      // TODO: check handle error
      if (typeof e === 'string') {
        throw new Error(e);
      }
      throw new Error('Something went wrong');
    }
  },
);

export const updateLink = createAsyncThunk(
  'clues/update-link',
  async ({ id, itemLink }: { id: number; itemLink: ItemLinkBase }) => {
    try {
      const { data } = await api.LinksApi.updateLinkModerationLinksLinkIdPut(id, itemLink);
      return data;
    } catch (e) {
      // TODO: check handle error
      if (typeof e === 'string') {
        throw new Error(e);
      }
      throw new Error('Something went wrong');
    }
  },
);

export const deleteLink = createAsyncThunk(
  'clues/delete-link',
  async ({ linkId, ownerId }: { linkId: number; ownerId: number }, thunkApi) => {
    try {
      await api.LinksApi.deleteLinkModerationLinkLinkIdDelete(linkId);
      thunkApi.dispatch(getClueById(ownerId));
      return linkId;
    } catch (e) {
      // TODO: check handle error
      if (typeof e === 'string') {
        throw new Error(e);
      }
      throw new Error('Something went wrong');
    }
  },
);
