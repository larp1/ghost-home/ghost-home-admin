import { RoleBase } from '@shared/api';

export type DataType = {
  id: number;
  key: React.Key;
  name: string;
  roles: RoleBase[];
  description: string;
};
