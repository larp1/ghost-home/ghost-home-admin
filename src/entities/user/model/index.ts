import * as slice from './slice';
import useTableUsers from './use-table-user';

export { slice, useTableUsers };
