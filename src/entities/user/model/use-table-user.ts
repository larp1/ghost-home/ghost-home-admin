import { useAppDispatch } from '@app';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import { actions, selectors } from './slice';

const useTableUsers = () => {
  const dispatch = useAppDispatch();
  const users = useSelector(selectors.selectUsers);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const getUsers = async () => {
      try {
        setLoading(true);
        await dispatch(actions.getAllUsers()).unwrap();
      } catch (err) {
        // TODO: remove console add toastr
        console.warn('error from get users', err);
      } finally {
        setLoading(false);
      }
    };

    getUsers();
  }, [dispatch]);

  return { users, loading };
};

export default useTableUsers;
