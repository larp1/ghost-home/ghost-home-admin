import { createSlice } from '@reduxjs/toolkit';
import { FullUserInfo } from '@shared/api';

import { createUser, getAllUsers, getUserById, removeUser, updateUser } from './async-actions';

type UserState = {
  items: FullUserInfo[];
};

export type UsersInStore = {
  users: UserState;
};

const initialState: UserState = {
  items: [],
};

const usersSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getAllUsers.fulfilled, (state, { payload }) => {
      state.items = payload;
    });

    builder.addCase(getUserById.fulfilled, (state, { payload }) => {
      const user = state.items.find((u) => u.id === payload.id);
      if (user) {
        user.name = payload.name;
        user.description = payload.description;
        user.roles = payload.roles;
        user.vk_link = payload.vk_link;
      }
    });

    builder.addCase(createUser.fulfilled, (state, { payload }) => {
      state.items.push(payload);
    });

    builder.addCase(updateUser.fulfilled, (state, { payload }) => {
      const user = state.items.find((u) => u.id === payload.id);
      if (user) {
        user.name = payload.name;
        user.description = payload.description;
        user.roles = payload.roles;
        user.vk_link = payload.vk_link;
      }
    });

    builder.addCase(removeUser.fulfilled, (state, { payload }) => {
      state.items = state.items.filter((item) => item.id !== payload);
    });
  },
});

export const selectors = {
  selectUsers: (state: UsersInStore) => state.users.items,
  selectUserById: (id: number) => (state: UsersInStore) => state.users.items.find((i) => i.id === id),
};
export const actions = {
  ...usersSlice.actions,
  createUser,
  getAllUsers,
  getUserById,
  removeUser,
  updateUser,
};
export const { reducer } = usersSlice;
