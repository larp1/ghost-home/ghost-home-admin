import { createAsyncThunk } from '@reduxjs/toolkit';
import { UserUpdate, UserWithRoleIds, api } from '@shared/api';

export const getAllUsers = createAsyncThunk('users/get-all-users', async () => {
  try {
    const { data } = await api.UsersApi.usersListModerationUsersGet();
    return data;
  } catch (e) {
    // TODO: check handle error
    if (typeof e === 'string') {
      throw new Error(e);
    }
    throw new Error('Something went wrong');
  }
});

export const getUserById = createAsyncThunk('users/get-user-by-id', async (userId: number) => {
  try {
    const { data } = await api.UsersApi.getUserModerationUserUserIdGet(userId);
    return data;
  } catch (e) {
    // TODO: check handle error
    if (typeof e === 'string') {
      throw new Error(e);
    }
    throw new Error('Something went wrong');
  }
});

export const createUser = createAsyncThunk(
  'users/create-user',
  async ({ roles, name, description, vk_link }: UserWithRoleIds) => {
    try {
      const { data } = await api.UsersApi.createUserModerationUserPost({ name, description, roles, vk_link });
      return data;
    } catch (e) {
      // TODO: check handle error
      if (typeof e === 'string') {
        throw new Error(e);
      }
      throw new Error('Something went wrong');
    }
  },
);

export const updateUser = createAsyncThunk(
  'users/update-user',
  async ({ description, name, roles, id, vk_link }: UserUpdate & { id: number }) => {
    try {
      await api.UsersApi.updateUserModerationUsersUserIdPut(id, {
        description,
        name,
        roles,
        vk_link,
      });
      const { data: newUser } = await api.UsersApi.getUserModerationUserUserIdGet(id);
      return newUser;
    } catch (e) {
      // TODO: check handle error
      if (typeof e === 'string') {
        throw new Error(e);
      }
      throw new Error('Something went wrong');
    }
  },
);

export const removeUser = createAsyncThunk('users/remove-user', async (userId: number) => {
  try {
    await api.UsersApi.deleteUserModerationUserUserIdDelete(userId);
    return userId;
  } catch (e) {
    // TODO: check handle error
    if (typeof e === 'string') {
      throw new Error(e);
    }
    throw new Error('Something went wrong');
  }
});
