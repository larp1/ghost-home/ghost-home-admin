import { CheckCircleTwoTone, CloseCircleTwoTone } from '@ant-design/icons';
import { Space, Table } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { FC, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import { useTableUsers } from '../model';
import { DataType } from '../types';
import { ActionRow } from './components';

const SCROLL_TABLE = { y: 'calc(100vh - 180px)' };

type TableUsersProps = {
  actions: FC<{ id: number }>[];
};

export const TableUsers: FC<TableUsersProps> = ({ actions }) => {
  const { t } = useTranslation();
  const { users, loading } = useTableUsers();

  const columns: ColumnsType<DataType> = useMemo(
    () => [
      {
        title: t('entities.user.table.name'),
        dataIndex: 'name',
        key: 'name',
        defaultSortOrder: 'descend',
        sorter: (a, b) => (a.name > b.name ? 1 : -1),
      },
      {
        title: t('entities.user.table.roles'),
        dataIndex: 'roles',
        key: 'roles',
        render: (_, record) => (
          <RolesWrapper>
            {record.roles.map((role, index) => (
              <span key={index.toString() + role}>{role.name}</span>
            ))}
          </RolesWrapper>
        ),
      },
      {
        title: t('entities.user.table.story'),
        render: (_, record) => (
          <Space size="small">
            {record.description ? (
              <CheckCircleTwoTone twoToneColor="#01ca01" />
            ) : (
              <CloseCircleTwoTone twoToneColor="#ff0000" />
            )}
          </Space>
        ),
      },
      {
        title: t('entities.role.table.actions'),
        dataIndex: '',
        key: 'action',
        render: (_, record) => (
          <Space size="large">{<ActionRow id={record.id} components={actions} />}</Space>
        ),
        width: '200px',
      },
    ],
    [t, actions],
  );

  const tableData: DataType[] = useMemo(() => {
    return users.map((user) => ({
      key: user.id,
      name: user.name,
      description: user.description,
      roles: user.roles,
      id: user.id,
    }));
  }, [users]);

  return (
    <Table
      columns={columns}
      dataSource={tableData}
      loading={loading}
      pagination={false}
      scroll={SCROLL_TABLE}
    />
  );
};

const RolesWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;
