import * as CluesFeatures from './clues';
import * as RolesFeatures from './roles';
import * as SwitchPages from './switch-pages';
import * as UsersFeatures from './users';

export { SwitchPages, RolesFeatures, UsersFeatures, CluesFeatures };
