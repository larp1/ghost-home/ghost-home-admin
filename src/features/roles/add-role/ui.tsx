import { Button, Input } from 'antd';
import React, { FC } from 'react';
import { Controller } from 'react-hook-form';
import styled from 'styled-components';

import { useAddRole } from './model';

const formRules = {
  name: { required: true },
};

type AddRoleProps = {
  className?: string;
};

export const AddRole: FC<AddRoleProps> = (props) => {
  const { control, isSubmitting, onAddRole } = useAddRole();

  return (
    <form onSubmit={onAddRole} {...props}>
      <StyledGroup compact>
        <Controller
          control={control}
          name="name"
          rules={formRules.name}
          render={({ field }) => <Input style={styles.input} {...field} />}
        />
        <Button type="primary" htmlType="submit" disabled={isSubmitting} loading={isSubmitting}>
          {!isSubmitting && '+'}
        </Button>
      </StyledGroup>
    </form>
  );
};

const styles: Record<string, React.CSSProperties> = {
  input: {
    width: '80%',
  },
};

const StyledGroup = styled(Input.Group)`
  width: 50%;
`;
