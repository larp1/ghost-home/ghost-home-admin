import { useAppDispatch } from '@app';
import { Role } from '@entities';
import { unwrapResult } from '@reduxjs/toolkit';
import { api } from '@shared/api';
import { useCallback } from 'react';
import { useForm } from 'react-hook-form';

import { AddFormType } from './types';

export const useAddRole = () => {
  const dispatch = useAppDispatch();
  const {
    control,
    handleSubmit,
    reset,
    formState: { isSubmitting },
  } = useForm<AddFormType>();

  const onAddRole = useCallback(
    async (values: AddFormType) => {
      try {
        await api.RolesApi.createRoleModerationRolePost({ name: values.name });
        const actionResult = await dispatch(Role.slice.actions.getRoles());
        unwrapResult(actionResult);
        reset({ name: '' });
      } catch (e) {
        // TODO: add toaster
        console.log('e', e);
      }
    },
    [dispatch, reset],
  );

  return { onAddRole: handleSubmit(onAddRole), control, isSubmitting };
};
