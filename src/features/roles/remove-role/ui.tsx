import { DeleteTwoTone } from '@ant-design/icons';
import { Role } from '@entities';
import { Button, Modal } from 'antd';
import React, { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { useRemoveRole } from './model';

type RemoveButtonProps = {
  id: number;
};

export const RemoveButton: FC<RemoveButtonProps> = ({ id }) => {
  const { t } = useTranslation();
  const role = useSelector(Role.slice.selectors.selectRolesById(id));
  const { modal, isRemoveLoading, handleRemove } = useRemoveRole(id);

  return (
    <>
      <Button icon={<DeleteTwoTone twoToneColor="#f72000" />} onClick={modal.open} />
      {modal.isVisible && (
        <Modal
          centered
          title={t('features.roles.modalRemove.title')}
          visible={modal.isVisible}
          okText={t('features.roles.modalRemove.ok')}
          onOk={handleRemove}
          confirmLoading={isRemoveLoading}
          cancelText={t('features.roles.modalRemove.cancel')}
          cancelButtonProps={{ disabled: isRemoveLoading }}
          onCancel={modal.close}>
          <p>
            {t('features.roles.modalRemove.description')}
            <b>{role?.name}</b>?
          </p>
        </Modal>
      )}
    </>
  );
};

export default RemoveButton;
