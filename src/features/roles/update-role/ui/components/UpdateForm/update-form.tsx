import { Form, Input } from 'antd';
import { FC } from 'react';
import { Control, Controller } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { UPDATE_FORM_ID } from '../../../config';
import { FormType } from '../../../types';

type UpdateFormProps = {
  isLoadingFulfilled: boolean;
  onSubmit: () => void;
  control: Control<FormType, unknown>;
};

export const UpdateForm: FC<UpdateFormProps> = ({ isLoadingFulfilled, onSubmit, control }) => {
  const { t } = useTranslation();

  return (
    <Form id={UPDATE_FORM_ID} layout="vertical" onSubmitCapture={onSubmit}>
      <Controller
        control={control}
        name="name"
        render={({ field }) => (
          <Form.Item label={t('features.roles.modalUpdate.inputs.title.label')} required>
            <Input
              placeholder={t('features.roles.modalUpdate.inputs.title.placeholder')}
              disabled={!isLoadingFulfilled}
              {...field}
            />
          </Form.Item>
        )}
      />
      <Controller
        control={control}
        name="description"
        render={({ field }) => (
          <Form.Item label={t('features.roles.modalUpdate.inputs.description.label')}>
            <Input.TextArea
              placeholder={t('features.roles.modalUpdate.inputs.description.placeholder')}
              disabled={!isLoadingFulfilled}
              {...field}
            />
          </Form.Item>
        )}
      />
    </Form>
  );
};
