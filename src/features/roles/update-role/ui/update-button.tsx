import { EditFilled } from '@ant-design/icons';
import { Button, Modal } from 'antd';
import { FC } from 'react';
import { useTranslation } from 'react-i18next';

import { UPDATE_FORM_ID } from '../config';
import { useUpdateRole } from '../model';
import { UpdateForm } from './components';

type UpdateButtonProps = {
  id: number;
};

export const UpdateButton: FC<UpdateButtonProps> = ({ id }) => {
  const { t } = useTranslation();
  const { modal, isLoading, isLoadingFulfilled, form } = useUpdateRole(id);

  return (
    <>
      <Button icon={<EditFilled />} onClick={modal.open} />
      {modal.isVisible && (
        <Modal
          centered
          title={t('features.roles.modalUpdate.title')}
          visible={modal.isVisible}
          okText={t('features.roles.modalUpdate.ok')}
          okButtonProps={{ htmlType: 'submit', form: UPDATE_FORM_ID }}
          confirmLoading={isLoading}
          cancelText={t('features.roles.modalUpdate.cancel')}
          cancelButtonProps={{ disabled: isLoading }}
          onCancel={modal.close}>
          <UpdateForm
            isLoadingFulfilled={isLoadingFulfilled}
            control={form.control}
            onSubmit={form.onSubmit}
          />
        </Modal>
      )}
    </>
  );
};
