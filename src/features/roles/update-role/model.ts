import { useAppDispatch } from '@app';
import { Role } from '@entities';
import { useState } from 'react';
import { useForm } from 'react-hook-form';

import { FormType } from './types';

export const useUpdateRole = (id: number) => {
  const dispatch = useAppDispatch();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isLoadingFulfilled, setIsLoadingFulfilled] = useState(false);

  const { control, handleSubmit, setValue } = useForm<FormType>({
    defaultValues: { name: '', description: '' },
  });

  const modalOpen = async () => {
    try {
      setIsModalVisible(true);
      setIsLoading(true);
      const data = await dispatch(Role.slice.actions.getRoleById(id)).unwrap();
      setValue('name', data.name);
      setValue('description', data.description || '');
      setIsLoadingFulfilled(true);
    } catch (e) {
      // TODO: add toastr
      alert('ERROR');
    } finally {
      setIsLoading(false);
    }
  };

  const onSubmit = async (values: FormType) => {
    try {
      setIsLoading(true);
      await dispatch(Role.slice.actions.updateRole({ ...values, id })).unwrap();
      setIsModalVisible(false);
    } catch (e) {
      // TODO: remove console.log
      console.warn('error update', e);
    } finally {
      setIsLoading(false);
    }
  };

  return {
    modal: {
      open: modalOpen,
      close: () => setIsModalVisible(false),
      isVisible: isModalVisible,
    },
    form: {
      control,
      onSubmit: handleSubmit(onSubmit),
    },
    isLoading,
    isLoadingFulfilled,
  };
};
