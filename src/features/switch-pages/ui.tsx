import { Tabs } from 'antd';
import { FC } from 'react';
import { useTranslation } from 'react-i18next';

import { pagesTabs } from './config';
import { useSwitchTabs } from './models';

export const SwitchPages: FC = () => {
  const { t } = useTranslation();
  const switchPage = useSwitchTabs();

  return (
    <Tabs onChange={switchPage}>
      {pagesTabs.map((tab) => (
        <Tabs.TabPane tab={t(tab.tKey) as string} key={tab.route} />
      ))}
    </Tabs>
  );
};
