import { KeysTranslation } from '@shared/i18n';
import { AppRoutes } from '@shared/routes';

export const pagesTabs: { tKey: KeysTranslation; route: AppRoutes }[] = [
  { tKey: 'features.switch-pages.roles', route: AppRoutes.RolesPage },
  { tKey: 'features.switch-pages.users', route: AppRoutes.UsersPage },
  { tKey: 'features.switch-pages.clues', route: AppRoutes.CluesPage },
];
