import { useNavigate } from 'react-router-dom';

export const useSwitchTabs = () => {
  const navigate = useNavigate();

  const switchPage = (key: string) => {
    navigate(key);
  };

  return switchPage;
};
