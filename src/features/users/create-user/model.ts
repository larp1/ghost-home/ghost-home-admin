import { useAppDispatch } from '@app';
import { User } from '@entities';
import { useState } from 'react';
import { useForm } from 'react-hook-form';

import { FormType } from './types';

export const useCreateUser = () => {
  const dispatch = useAppDispatch();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const { control, handleSubmit, reset } = useForm<FormType>({
    defaultValues: { name: '', description: '', roles: [], vk_link: '' },
  });

  const onSubmit = async (values: FormType) => {
    try {
      setIsLoading(true);
      await dispatch(
        User.slice.actions.createUser({
          name: values.name,
          description: values.description,
          roles: { ids: values.roles },
          vk_link: values.vk_link,
        }),
      ).unwrap();
      reset();
      setIsModalVisible(false);
    } catch (e) {
      // TODO: remove console.log
      console.warn('error update', e);
    } finally {
      setIsLoading(false);
    }
  };

  return {
    modal: {
      open: () => setIsModalVisible(true),
      close: () => setIsModalVisible(false),
      isVisible: isModalVisible,
    },
    form: {
      control,
      onSubmit: handleSubmit(onSubmit),
    },
    isLoading,
  };
};
