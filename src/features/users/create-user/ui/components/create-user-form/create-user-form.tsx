import { Role } from '@entities';
import { Checkbox, Form, Input } from 'antd';
import React, { FC } from 'react';
import { useMemo } from 'react';
import { Control, Controller } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { CREATE_FORM_ID } from '../../../config';
import { FormType } from '../../../types';

type CreateUserFormProps = {
  onSubmit: () => void;
  control: Control<FormType, unknown>;
};

export const CreateUserForm: FC<CreateUserFormProps> = ({ control, onSubmit }) => {
  const { t } = useTranslation();
  const roles = useSelector(Role.slice.selectors.selectRoles);

  const options = useMemo(() => {
    return roles.map((role) => ({ label: role.name, value: role.id }));
  }, [roles]);

  return (
    <Form id={CREATE_FORM_ID} layout="vertical" onSubmitCapture={onSubmit}>
      <Controller
        control={control}
        name="name"
        render={({ field }) => (
          <Form.Item label={t('features.users.modalCreate.inputs.name.label')} required>
            <Input placeholder={t('features.users.modalCreate.inputs.name.placeholder')} {...field} />
          </Form.Item>
        )}
      />
      <Controller
        control={control}
        name="vk_link"
        render={({ field }) => (
          <Form.Item label={t('features.users.modalCreate.inputs.vk_link.label')}>
            <Input placeholder={t('features.users.modalCreate.inputs.vk_link.placeholder')} {...field} />
          </Form.Item>
        )}
      />
      <Controller
        control={control}
        name="description"
        render={({ field }) => (
          <Form.Item label={t('features.users.modalCreate.inputs.description.label')}>
            <Input.TextArea
              rows={15}
              placeholder={t('features.users.modalCreate.inputs.description.placeholder')}
              {...field}
            />
          </Form.Item>
        )}
      />
      <Controller
        control={control}
        name="roles"
        render={({ field }) => (
          <Checkbox.Group value={field.value} options={options} onChange={field.onChange} />
        )}
      />
    </Form>
  );
};
