import { PlusCircleOutlined } from '@ant-design/icons';
import { Button, Modal } from 'antd';
import { FC } from 'react';
import { useTranslation } from 'react-i18next';

import { CREATE_FORM_ID } from '../config';
import { useCreateUser } from '../model';
import { CreateUserForm } from './components';

export const CreateUser: FC = () => {
  const { t } = useTranslation();
  const { modal, form, isLoading } = useCreateUser();

  return (
    <>
      <Button size="large" type="primary" icon={<PlusCircleOutlined />} onClick={modal.open} />
      {modal.isVisible && (
        <Modal
          width="90%"
          centered
          title={t('features.users.modalCreate.title')}
          visible={modal.isVisible}
          okText={t('features.users.modalCreate.ok')}
          okButtonProps={{ htmlType: 'submit', form: CREATE_FORM_ID }}
          confirmLoading={isLoading}
          cancelText={t('features.users.modalCreate.cancel')}
          cancelButtonProps={{ disabled: isLoading }}
          onCancel={modal.close}>
          <CreateUserForm control={form.control} onSubmit={form.onSubmit} />
        </Modal>
      )}
    </>
  );
};
