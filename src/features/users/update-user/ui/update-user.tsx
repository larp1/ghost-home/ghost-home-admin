import { EditFilled } from '@ant-design/icons';
import { Button, Modal } from 'antd';
import React, { FC } from 'react';
import { useTranslation } from 'react-i18next';

import { UPDATE_USER_FORM_ID } from '../config';
import { useUpdateUser } from '../model';
import { UpdateUserForm } from './components';

type UpdateUserProps = {
  id: number;
};

export const UpdateUser: FC<UpdateUserProps> = ({ id }) => {
  const { t } = useTranslation();
  const { modal, form, isLoading } = useUpdateUser(id);
  return (
    <>
      <Button icon={<EditFilled />} onClick={modal.open} />
      {modal.isVisible && (
        <Modal
          width="90%"
          centered
          title={t('features.users.modalUpdate.title')}
          visible={modal.isVisible}
          okText={t('features.users.modalUpdate.ok')}
          okButtonProps={{ htmlType: 'submit', form: UPDATE_USER_FORM_ID }}
          confirmLoading={isLoading}
          cancelText={t('features.users.modalUpdate.cancel')}
          cancelButtonProps={{ disabled: isLoading }}
          onCancel={modal.close}>
          <UpdateUserForm control={form.control} onSubmit={form.onSubmit} />
        </Modal>
      )}
    </>
  );
};
