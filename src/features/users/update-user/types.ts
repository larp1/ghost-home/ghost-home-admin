export type FormType = {
  name: string;
  description: string;
  roles: number[];
  vk_link?: string;
};
