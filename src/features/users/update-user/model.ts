import { useAppDispatch } from '@app';
import { Role, User } from '@entities';
import { sleep } from '@shared/helpers/sleep';
import { useMemo, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useSelector } from 'react-redux';

import { FormType } from './types';

export const useUpdateUser = (id: number) => {
  const dispatch = useAppDispatch();
  const user = useSelector(User.slice.selectors.selectUserById(id));
  const rolesMap = useSelector(Role.slice.selectors.selectRolesMap);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const defaultRolesValues = useMemo(() => user?.roles.map((role) => rolesMap[role.name]), [user, rolesMap]);

  const { control, handleSubmit } = useForm<FormType>({
    defaultValues: {
      name: user?.name || '',
      description: user?.description || '',
      roles: defaultRolesValues || [],
      vk_link: user?.vk_link || '',
    },
  });

  const onSubmit = async (values: FormType) => {
    try {
      setIsLoading(true);
      await sleep();
      await dispatch(
        User.slice.actions.updateUser({
          id,
          name: values.name,
          description: values.description,
          roles: { ids: values.roles },
          vk_link: values.vk_link,
        }),
      ).unwrap();
      setIsModalVisible(false);
    } catch (err) {
      // TODO: remove console.log
      console.warn('error update', err);
    } finally {
      setIsLoading(false);
    }
  };

  return {
    modal: {
      open: () => setIsModalVisible(true),
      close: () => setIsModalVisible(false),
      isVisible: isModalVisible,
    },
    form: {
      control,
      onSubmit: handleSubmit(onSubmit),
    },
    isLoading,
  };
};
