import { DeleteTwoTone } from '@ant-design/icons';
import { User } from '@entities';
import { Button, Modal } from 'antd';
import React, { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { useRemoveUser } from './model';

type RemoveUserType = {
  id: number;
};

export const RemoveUser: FC<RemoveUserType> = ({ id }) => {
  const { t } = useTranslation();
  const user = useSelector(User.slice.selectors.selectUserById(id));
  const { modal, handleRemove, isRemoveLoading } = useRemoveUser(id);

  return (
    <>
      <Button icon={<DeleteTwoTone twoToneColor="#f72000" />} onClick={modal.open} />
      {modal.isVisible && (
        <Modal
          centered
          title={t('features.users.modalRemove.title')}
          visible={modal.isVisible}
          okText={t('features.users.modalRemove.ok')}
          onOk={handleRemove}
          confirmLoading={isRemoveLoading}
          cancelText={t('features.users.modalRemove.cancel')}
          cancelButtonProps={{ disabled: isRemoveLoading }}
          onCancel={modal.close}>
          <p>
            {t('features.users.modalRemove.description')}
            <b>{user?.name}</b>?
          </p>
        </Modal>
      )}
    </>
  );
};
