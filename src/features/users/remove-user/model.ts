import { useAppDispatch } from '@app';
import { User } from '@entities';
import { useState } from 'react';

export const useRemoveUser = (id: number) => {
  const dispatch = useAppDispatch();
  const [isVisibleModal, setIsVisibleModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const handleRemove = async () => {
    try {
      setIsLoading(true);
      await dispatch(User.slice.actions.removeUser(id));
      setIsVisibleModal(false);
    } catch (err) {
      // TODO: add error handler
      console.warn('error in remove', err);
    } finally {
      setIsLoading(false);
    }
  };

  return {
    modal: {
      open: () => setIsVisibleModal(true),
      close: () => setIsVisibleModal(false),
      isVisible: isVisibleModal,
    },
    handleRemove,
    isRemoveLoading: isLoading,
  };
};
