import { FacebookFilled } from '@ant-design/icons';
import { Button } from 'antd';
import React, { FC } from 'react';

import { useGoToGamerProfile } from './model';

type GoGamerProfileProps = {
  id: number;
};
// TODO: add logic for link to social profile
export const GoGamerProfile: FC<GoGamerProfileProps> = ({ id }) => {
  const onClick = useGoToGamerProfile(id);

  return <Button icon={<FacebookFilled />} onClick={onClick} />;
};
