import { User } from '@entities';
import { useSelector } from 'react-redux';

export const useGoToGamerProfile = (id: number) => {
  const user = useSelector(User.slice.selectors.selectUserById(id));

  const onClickProfileButton = () => {
    if (user?.vk_link) {
      window.open(user?.vk_link, '_blank');
    }
  };

  return onClickProfileButton;
};
