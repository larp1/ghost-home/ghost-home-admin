import { QrcodeOutlined } from '@ant-design/icons';
import { Button, Modal } from 'antd';
import QRCode from 'qrcode.react';
import React, { FC } from 'react';

import { useQRCodeUser } from '../model';

type QRButtonProps = {
  id: number;
};

const FOOTER = [] as React.ReactNode[];

export const QRButton: FC<QRButtonProps> = ({ id }) => {
  const { user, modal, downloadQRCode } = useQRCodeUser(id);

  return (
    <>
      <Button icon={<QrcodeOutlined />} onClick={modal.open} />
      <Modal centered title={user?.name} visible={modal.isVisible} footer={FOOTER} onCancel={modal.close}>
        <div>
          <QRCode
            id={user?.id.toString()}
            value={`${user?.id}`}
            size={290}
            level={'H'}
            includeMargin={true}
          />
          <p>
            <button type="button" onClick={downloadQRCode}>
              Download QR Code
            </button>
          </p>
        </div>
      </Modal>
    </>
  );
};
