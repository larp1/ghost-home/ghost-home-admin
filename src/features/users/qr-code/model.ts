import { User } from '@entities';
import { useState } from 'react';
import { useSelector } from 'react-redux';

export const useQRCodeUser = (id: number) => {
  const user = useSelector(User.slice.selectors.selectUserById(id));
  const [isVisibleModal, setIsVisibleModal] = useState(false);

  const downloadQRCode = () => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const canvas = document.getElementById(user!.id.toString());
    // @ts-ignore
    const pngUrl = canvas?.toDataURL('image/png').replace('image/png', 'image/octet-stream');
    const downloadLink = document.createElement('a');
    downloadLink.href = pngUrl;
    downloadLink.download = `${user?.name}.png`;
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
  };

  return {
    modal: {
      open: () => setIsVisibleModal(true),
      close: () => setIsVisibleModal(false),
      isVisible: isVisibleModal,
    },
    user,
    downloadQRCode,
  };
};
