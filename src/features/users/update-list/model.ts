import { useAppDispatch } from '@app';
import { User } from '@entities';
import { useCallback, useState } from 'react';

export const useUpdateList = () => {
  const dispatch = useAppDispatch();
  const [loading, setLoading] = useState(false);

  const updateList = useCallback(async () => {
    try {
      setLoading(true);
      await dispatch(User.slice.actions.getAllUsers()).unwrap();
    } catch (err) {
      // TODO: remove log and add toastr
    } finally {
      setLoading(false);
    }
  }, [dispatch]);

  return { updateList, loading };
};
