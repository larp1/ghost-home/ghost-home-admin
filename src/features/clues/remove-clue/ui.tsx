import { DeleteTwoTone } from '@ant-design/icons';
import { Clue } from '@entities';
import { Button, Modal } from 'antd';
import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { useRemoveClue } from './model';

type RemoveClueType = {
  id: number;
};

export const RemoveClue: FC<RemoveClueType> = ({ id }) => {
  const { t } = useTranslation();
  const clue = useSelector(Clue.slice.selectors.selectClueById(id));
  const { modal, handleRemove, isRemoveLoading } = useRemoveClue(id);

  return (
    <>
      <Button icon={<DeleteTwoTone twoToneColor="#f72000" />} onClick={modal.open} />
      {modal.isVisible && (
        <Modal
          centered
          title={t('features.clues.modalRemove.title')}
          visible={modal.isVisible}
          okText={t('features.clues.modalRemove.ok')}
          onOk={handleRemove}
          confirmLoading={isRemoveLoading}
          cancelText={t('features.clues.modalRemove.cancel')}
          cancelButtonProps={{ disabled: isRemoveLoading }}
          onCancel={modal.close}>
          <p>
            {t('features.clues.modalRemove.description')}
            <b>{clue?.title}</b>?
          </p>
        </Modal>
      )}
    </>
  );
};
