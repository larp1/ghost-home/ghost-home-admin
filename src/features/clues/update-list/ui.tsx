import { ReloadOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import React, { FC } from 'react';

import { useUpdateList } from './model';

export const UpdateList: FC = () => {
  const { loading, updateList } = useUpdateList();

  return <Button size="large" icon={<ReloadOutlined />} disabled={loading} onClick={updateList} />;
};
