import { useAppDispatch } from '@app';
import { Clue } from '@entities';
import { useCallback, useState } from 'react';

export const useUpdateList = () => {
  const dispatch = useAppDispatch();
  const [loading, setLoading] = useState(false);

  const updateList = useCallback(async () => {
    try {
      setLoading(true);
      await dispatch(Clue.slice.actions.getAllClues()).unwrap();
    } catch (err) {
      // alert(JSON.stringify(err));
    } finally {
      setLoading(false);
    }
  }, [dispatch]);

  return { updateList, loading };
};
