export type FormType = {
  role: number;
  text: string;
  title: string;
  madnes?: number;
  math?: string;
};
