import { useAppDispatch } from '@app';
import { Clue } from '@entities';
import { useState } from 'react';
import { useForm } from 'react-hook-form';

import { FormType } from './types';

export const useCreateClue = () => {
  const dispatch = useAppDispatch();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const { control, handleSubmit, reset, setError } = useForm<FormType>({
    defaultValues: { title: '', math: '', text: '', madnes: undefined, role: undefined },
  });

  const onSubmit = async (values: FormType) => {
    if (isNaN(Number(values.role))) {
      setError('role', { message: 'is NaN' });
      return;
    }
    if (isNaN(Number(values.madnes)) && values.madnes) {
      setError('madnes', { message: 'is NaN' });
      return;
    }

    try {
      setIsLoading(true);
      await dispatch(
        Clue.slice.actions.createClue({
          title: values.title,
          text: values.text,
          role: Number(values.role),
          math: values.math,
          madnes: Number(values.madnes),
          links: [],
        }),
      ).unwrap();
      reset();
      setIsModalVisible(false);
    } catch (e) {
      // TODO: remove console.log
      console.warn('error update', e);
    } finally {
      setIsLoading(false);
    }
  };

  return {
    modal: {
      open: () => setIsModalVisible(true),
      close: () => setIsModalVisible(false),
      isVisible: isModalVisible,
    },
    form: {
      control,
      onSubmit: handleSubmit(onSubmit),
    },
    isLoading,
  };
};
