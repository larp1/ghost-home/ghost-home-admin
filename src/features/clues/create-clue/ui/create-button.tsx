import { PlusCircleOutlined } from '@ant-design/icons';
import { Button, Modal } from 'antd';
import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import { CREATE_FORM_ID } from '../config';
import { useCreateClue } from '../model';
import { CreateClueForm } from './components';

export const CreateClue: FC = () => {
  const { t } = useTranslation();
  const { modal, form, isLoading } = useCreateClue();

  return (
    <>
      <Button size="large" type="primary" icon={<PlusCircleOutlined />} onClick={modal.open} />
      {modal.isVisible && (
        <Modal
          width="90%"
          centered
          title={t('features.clues.modalCreate.title')}
          visible={modal.isVisible}
          okText={t('features.clues.modalCreate.ok')}
          okButtonProps={{ htmlType: 'submit', form: CREATE_FORM_ID }}
          confirmLoading={isLoading}
          cancelText={t('features.clues.modalCreate.cancel')}
          cancelButtonProps={{ disabled: isLoading }}
          onCancel={modal.close}>
          <CreateClueForm control={form.control} onSubmit={form.onSubmit} />
        </Modal>
      )}
    </>
  );
};

const AddLink = styled.div`
  margin-top: 10px;
`;
