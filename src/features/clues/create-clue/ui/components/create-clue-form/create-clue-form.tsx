import { Role } from '@entities';
import { Form, Input, Select } from 'antd';
import React, { FC } from 'react';
import { useMemo } from 'react';
import { Control, Controller } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import { CREATE_FORM_ID } from '../../../config';
import { FormType } from '../../../types';

type CreateClueFormProps = {
  onSubmit: () => void;
  control: Control<FormType, unknown>;
};

export const CreateClueForm: FC<CreateClueFormProps> = ({ control, onSubmit }) => {
  const { t } = useTranslation();
  const roles = useSelector(Role.slice.selectors.selectRoles);

  const options = useMemo(() => {
    return roles.map((role) => ({ label: role.name, value: role.id }));
  }, [roles]);

  return (
    <Form id={CREATE_FORM_ID} layout="vertical" onSubmitCapture={onSubmit}>
      <Controller
        control={control}
        name="title"
        rules={{ required: true }}
        render={({ field }) => (
          <Form.Item label={t('features.clues.modalCreate.inputs.title.label')} required>
            <Input placeholder={t('features.clues.modalCreate.inputs.title.placeholder')} {...field} />
          </Form.Item>
        )}
      />
      <Controller
        control={control}
        name="math"
        render={({ field }) => (
          <Form.Item label={t('features.clues.modalCreate.inputs.math.label')}>
            <Input placeholder={t('features.clues.modalCreate.inputs.math.placeholder')} {...field} />
          </Form.Item>
        )}
      />
      <Controller
        control={control}
        name="text"
        rules={{ required: true }}
        render={({ field }) => (
          <Form.Item label={t('features.clues.modalCreate.inputs.description.label')} required>
            <Input.TextArea
              rows={5}
              placeholder={t('features.clues.modalCreate.inputs.description.placeholder')}
              {...field}
            />
          </Form.Item>
        )}
      />
      <Controller
        control={control}
        name="madnes"
        render={({ field }) => (
          <Form.Item label={t('features.clues.modalCreate.inputs.madness.label')}>
            <Input
              type="number"
              placeholder={t('features.clues.modalCreate.inputs.madness.placeholder')}
              {...field}
            />
          </Form.Item>
        )}
      />
      <Controller
        control={control}
        name="role"
        rules={{ required: true }}
        render={({ field }) => (
          <>
            <span>
              <Red>* </Red>
              {t('features.clues.modalCreate.inputs.role.label')}{' '}
            </span>
            <Select placeholder="Select a person" onChange={field.onChange} value={field.value}>
              {options.map((option) => (
                <Select.Option key={option.value} value={option.value}>
                  {option.label}
                </Select.Option>
              ))}
            </Select>
          </>
        )}
      />
    </Form>
  );
};

const Red = styled.span`
  color: red;
`;
