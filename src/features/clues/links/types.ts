export type FormType = {
  linked_id: number;
  title: string;
};
