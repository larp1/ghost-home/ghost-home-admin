import { useAppDispatch } from '@app';
import { Clue } from '@entities';
import { useState } from 'react';
import { useForm } from 'react-hook-form';

import { FormType } from './types';

export const useManageLinks = (id: number) => {
  const dispatch = useAppDispatch();
  const [isVisibleModal, setIsVisibleModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const { control, handleSubmit, reset } = useForm<FormType>({
    defaultValues: {},
  });

  const onSubmit = async (values: FormType) => {
    try {
      setIsLoading(true);
      await dispatch(
        Clue.slice.actions.createLink({ linked_id: values.linked_id, title: values.title, owner_id: id }),
      );
      reset();
      setIsVisibleModal(false);
    } catch (err) {
      // TODO: remove log add toastr
      console.log('err', err);
    } finally {
      setIsLoading(false);
    }
  };

  const removeLink = async (linkId: number) => {
    try {
      setIsLoading(true);
      await dispatch(Clue.slice.actions.deleteLink({ linkId, ownerId: id }));
    } catch (err) {
      // TODO: remove log add toastr
      console.log('err', err);
    } finally {
      setIsLoading(false);
    }
  };

  return {
    modal: {
      open: () => setIsVisibleModal(true),
      close: () => setIsVisibleModal(false),
      isVisible: isVisibleModal,
    },
    form: {
      control,
      onSubmit: handleSubmit(onSubmit),
    },
    isLoading,
    removeLink,
  };
};
