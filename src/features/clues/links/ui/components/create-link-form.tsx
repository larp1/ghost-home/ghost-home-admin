import { useAppDispatch } from '@app';
import { Clue } from '@entities';
import { Form, Input, Select } from 'antd';
import { FC, useEffect, useMemo } from 'react';
import { Control, Controller } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import { CREATE_LINK } from '../../config';
import { FormType } from '../../types';

type CreateLinkFormProps = {
  onSubmit: () => void;
  control: Control<FormType, unknown>;
  id: number;
};

export const CreateLinkForm: FC<CreateLinkFormProps> = ({ control, onSubmit, id }) => {
  const dispatch = useAppDispatch();
  const { t } = useTranslation();
  const clues = useSelector(Clue.slice.selectors.selectClues);

  useEffect(() => {
    dispatch(Clue.slice.actions.getClueById(id));
  }, [id, dispatch]);

  const options = useMemo(() => {
    return clues
      .filter((clue) => clue.id !== id)
      .map((clue) => ({ label: clue.title, value: clue.id }))
      .reverse();
  }, [clues, id]);

  return (
    <Form id={CREATE_LINK} layout="vertical" onSubmitCapture={onSubmit}>
      <Controller
        control={control}
        name="title"
        rules={{ required: true }}
        render={({ field }) => (
          <Form.Item label={t('features.clues.modalLink.inputs.label')} required>
            <Input placeholder={t('features.clues.modalLink.inputs.placeholder')} {...field} />
          </Form.Item>
        )}
      />
      <Controller
        control={control}
        name="linked_id"
        rules={{ required: true }}
        render={({ field }) => (
          <>
            <span>
              <Red>* </Red>
              {t('features.clues.modalLink.linkTo')}{' '}
            </span>
            <Select placeholder="Select a person" onChange={field.onChange} value={field.value}>
              {options.map((option) => (
                <Select.Option key={option.value} value={option.value}>
                  {option.label}
                </Select.Option>
              ))}
            </Select>
          </>
        )}
      />
    </Form>
  );
};

const Red = styled.span`
  color: red;
`;
