import { CloseCircleTwoTone, LinkOutlined } from '@ant-design/icons';
import { Clue } from '@entities';
import { Button, Modal } from 'antd';
import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import styled from 'styled-components';

import { CREATE_LINK } from '../config';
import { useManageLinks } from '../model';
import { CreateLinkForm } from './components/create-link-form';

type ManageLinksProps = {
  id: number;
};

export const ManageLinks: FC<ManageLinksProps> = ({ id }) => {
  const { t } = useTranslation();
  const { modal, form, isLoading, removeLink } = useManageLinks(id);
  const clue = useSelector(Clue.slice.selectors.selectClueById(id));
  const cluesMap = useSelector(Clue.slice.selectors.selectCluesMap);

  return (
    <>
      <Button icon={<LinkOutlined />} onClick={modal.open} />
      {modal.isVisible && (
        <Modal
          width="90%"
          centered
          title={t('features.clues.modalLink.title', { title: clue?.title })}
          visible={modal.isVisible}
          okText={t('features.clues.modalLink.ok')}
          okButtonProps={{ htmlType: 'submit', form: CREATE_LINK }}
          confirmLoading={isLoading}
          cancelText={t('features.clues.modalLink.cancel')}
          cancelButtonProps={{ disabled: isLoading }}
          onCancel={modal.close}>
          <>
            <ol>
              {clue?.links?.map((link) => (
                <Row key={link.id}>
                  {link.title} --- {cluesMap[link.linked_id]}{' '}
                  <Button
                    icon={<CloseCircleTwoTone twoToneColor="#ff0000" />}
                    onClick={() => removeLink(link.id)}
                  />
                </Row>
              ))}
            </ol>
            <CreateLinkForm control={form.control} onSubmit={form.onSubmit} id={id} />
          </>
        </Modal>
      )}
    </>
  );
};

const Row = styled.li`
  margin-bottom: 10px;
`;
