import { useAppDispatch } from '@app';
import { Clue } from '@entities';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { useSelector } from 'react-redux';

import { FormType } from './types';

export const useUpdateClue = (id: number) => {
  const dispatch = useAppDispatch();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const clue = useSelector(Clue.slice.selectors.selectClueById(id));

  const { control, handleSubmit, setError } = useForm<FormType>({
    defaultValues: {
      title: clue?.title || '',
      math: clue?.math || '',
      text: clue?.text || '',
      madnes: clue?.madnes,
      role: clue?.role,
    },
  });

  const onSubmit = async (values: FormType) => {
    if (isNaN(Number(values.role))) {
      setError('role', { message: 'is NaN' });
      return;
    }
    if (isNaN(Number(values.madnes)) && values.madnes) {
      setError('madnes', { message: 'is NaN' });
      return;
    }

    try {
      setIsLoading(true);
      await dispatch(
        Clue.slice.actions.updateClue({
          id,
          itemUpdate: {
            title: values.title,
            text: values.text,
            role: Number(values.role),
            math: values.math,
            madnes: Number(values.madnes),
          },
        }),
      ).unwrap();
      setIsModalVisible(false);
    } catch (e) {
      // TODO: remove console.log
      console.warn('error update', e);
    } finally {
      setIsLoading(false);
    }
  };

  return {
    modal: {
      open: () => setIsModalVisible(true),
      close: () => setIsModalVisible(false),
      isVisible: isModalVisible,
    },
    form: {
      control,
      onSubmit: handleSubmit(onSubmit),
    },
    isLoading,
  };
};
