import { EditFilled } from '@ant-design/icons';
import { Button, Modal } from 'antd';
import { FC } from 'react';
import { useTranslation } from 'react-i18next';

import { UPDATE_FORM_ID } from '../config';
import { useUpdateClue } from '../model';
import { UpdateClueForm } from './components';

type UpdateClueProps = {
  id: number;
};

export const UpdateClue: FC<UpdateClueProps> = ({ id }) => {
  const { t } = useTranslation();
  const { modal, form, isLoading } = useUpdateClue(id);

  return (
    <>
      <Button icon={<EditFilled />} onClick={modal.open} />
      {modal.isVisible && (
        <Modal
          width="90%"
          centered
          title={t('features.clues.modalUpdate.title')}
          visible={modal.isVisible}
          okText={t('features.clues.modalUpdate.ok')}
          okButtonProps={{ htmlType: 'submit', form: UPDATE_FORM_ID }}
          confirmLoading={isLoading}
          cancelText={t('features.clues.modalUpdate.cancel')}
          cancelButtonProps={{ disabled: isLoading }}
          onCancel={modal.close}>
          <UpdateClueForm control={form.control} onSubmit={form.onSubmit} />
        </Modal>
      )}
    </>
  );
};
