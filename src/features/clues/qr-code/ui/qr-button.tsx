import { QrcodeOutlined } from '@ant-design/icons';
import { Button, Modal } from 'antd';
import QRCode from 'qrcode.react';
import React, { FC } from 'react';

import { useQRCodeClue } from '../model';

type QRButtonProps = {
  id: number;
};

const FOOTER = [] as React.ReactNode[];

export const QRButton: FC<QRButtonProps> = ({ id }) => {
  const { clue, modal, downloadQRCode } = useQRCodeClue(id);

  return (
    <>
      <Button icon={<QrcodeOutlined />} onClick={modal.open} />
      <Modal centered title={clue?.title} visible={modal.isVisible} footer={FOOTER} onCancel={modal.close}>
        <div>
          <QRCode
            id={clue?.id.toString()}
            value={`/item/${clue?.id}`}
            size={290}
            level={'H'}
            includeMargin={true}
          />
          <p>
            <button type="button" onClick={downloadQRCode}>
              Download QR Code
            </button>
          </p>
        </div>
      </Modal>
    </>
  );
};
