import { Clue } from '@entities';
import { useState } from 'react';
import { useSelector } from 'react-redux';

export const useQRCodeClue = (id: number) => {
  const clue = useSelector(Clue.slice.selectors.selectClueById(id));
  const [isVisibleModal, setIsVisibleModal] = useState(false);

  const downloadQRCode = () => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const canvas = document.getElementById(clue!.id.toString());
    // @ts-ignore
    const pngUrl = canvas?.toDataURL('image/png').replace('image/png', 'image/octet-stream');
    const downloadLink = document.createElement('a');
    downloadLink.href = pngUrl;
    downloadLink.download = `${clue?.title}.png`;
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
  };

  return {
    modal: {
      open: () => setIsVisibleModal(true),
      close: () => setIsVisibleModal(false),
      isVisible: isVisibleModal,
    },
    clue,
    downloadQRCode,
  };
};
