import { HTTPValidationError, httpClient } from '@shared/api';
import { AxiosError } from 'axios';

export const AxiosInterceptors = {
  setup: () => {
    // ***** REQUEST interceptor *****
    httpClient.interceptors.request.use(
      async (config) => {
        return config;
      },
      (error) => {
        return Promise.reject(error);
      },
    );
    // ***** RESPONSE interceptor *****
    httpClient.interceptors.response.use(
      (response) => {
        return response;
      },
      (error: AxiosError<HTTPValidationError>) => {
        if (error.response?.data.detail) {
          const message = error.response.data.detail.map((item) => item.msg).join(', ');
          throw message;
        }
        throw error.message;
      },
    );
  },
};
