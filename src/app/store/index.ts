import { Action, AnyAction, ThunkAction, configureStore } from '@reduxjs/toolkit';
import { ENVIRONMENT, EnvironmentType } from '@shared/constants';
import { useDispatch } from 'react-redux';
import {
  FLUSH,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
  REHYDRATE,
  persistReducer,
  persistStore,
} from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import { reducer } from './ducks';

type State = ReturnType<typeof reducer>;

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['auth', 'settings'],
};

const rootReducer = (state: State, action: AnyAction) => {
  return reducer(state, action);
};

const persistedReducer = persistReducer(persistConfig, rootReducer as typeof reducer);

export const store = configureStore({
  reducer: persistedReducer,
  // @ts-ignore
  devTools: ENVIRONMENT === EnvironmentType.development,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
});

export const persistor = persistStore(store);

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;
export const useAppDispatch = () => useDispatch<AppDispatch>();
