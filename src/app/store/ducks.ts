import { Clue, Role, User } from '@entities';
import { combineReducers } from 'redux';

type Slices = Role.slice.RolesInStore & User.slice.UsersInStore & Clue.slice.CluesInStore;

export const reducer = combineReducers<Slices>({
  roles: Role.slice.reducer,
  users: User.slice.reducer,
  clues: Clue.slice.reducer,
});
