import 'antd/dist/antd.css';
import '@shared/i18n';

import './styles.css';

import { Navigation } from '@processes';
import defaultTheme from '@shared/themes';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { ThemeProvider } from 'styled-components';

import { AxiosInterceptors } from './axios-interceptors';
import { persistor, store, useAppDispatch } from './store';

AxiosInterceptors.setup();

function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={<span>Loading...</span>} persistor={persistor}>
        <ThemeProvider theme={defaultTheme}>
          <Navigation.Navigator />
        </ThemeProvider>
      </PersistGate>
    </Provider>
  );
}

export default App;

export { useAppDispatch };
