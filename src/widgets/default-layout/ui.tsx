import { SwitchPages } from '@features';
import { FC } from 'react';
import { Outlet } from 'react-router-dom';
import styled from 'styled-components';

export const DefaultLayout: FC = () => {
  return (
    <Root>
      <SwitchPages.UI />
      <Outlet />
    </Root>
  );
};

const Root = styled.div`
  padding: 0 20px;
`;
